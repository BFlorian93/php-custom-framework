# Simple PHP Framework

## Requirements

* [PHP](http://php.net/)
* [MySQL](https://www.mysql.com/)
* [Composer](https://getcomposer.org/)

## Installation

* Download the [repository](https://bitbucket.org/BFlorian93/php-custom-framework/get/594e1391b241.zip)
* Install composer: `composer install`
* Run `composer dump-autoload`
* Set the `database` in `config.php`
* Start the server: `php -S localhost:8000`